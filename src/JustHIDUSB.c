#include "chip.h"
#include "string.h"
#include "stdbool.h"

#include "usb_definitions.h"
#include "usb_initialization.h"
#include "usb_descriptors.h"
#include "usb_routines.h"

#include "delay_routines.h"
#include <cr_section_macros.h>

#define DEBUG_USB

uint8_t receivedData[64];
uint8_t sendData[64];
uint16_t addressAssignedByHost = 0;

#ifdef DEBUG_USB
uint8_t debugStrings[20][2];
uint8_t debugStringCounter = 0;

void logDebugMessage(char* message) {
	strcpy(debugStrings[debugStringCounter], message);
	debugStringCounter += 1;
}

void logDebugRequest(uint8_t requestType, uint8_t request) {
	debugStrings[debugStringCounter][0] = requestType;
	debugStrings[debugStringCounter][1] = request;
	debugStringCounter += 1;
}
#endif

bool clearBuffer(void) {
	bool isPacketOverwritten = readFromSIE(CLEAR_BUFFER_COMMAND);
	return isPacketOverwritten;
}

uint8_t getSIEerrorCode(void) {
	return readFromSIE(SIE_ERROR_CODE_COMMAND) & 0xF; // only lower 4 bits have anything of interest
}

uint8_t selectEndpointClearInterrupt(uint8_t physicalEndpointIndex) {
	uint8_t endpointInformation = readFromSIE(SELECT_ENDPOINT_COMMAND + physicalEndpointIndex);
	return endpointInformation;
}

void validateTxBuffer(void) {
	writeToSIEWithoutData(VALIDATE_BUFFER_COMMAND_CODE);
}

void enableNoncontrolEndpoints(void) {
	writeToSIE(ENABLE_NON_CONTROL_ENDPOINTS, CONFIGURE_DEVICE);
}

uint8_t selectPhysicalEndpoint(uint8_t physicalEndpointIndex) {
	return readFromSIE(physicalEndpointIndex);
}

uint8_t* readDataFromEndpoint(uint8_t logicalEndpoint) {
	uint32_t counter = 0;

	memset(receivedData, 0, 64 * sizeof(uint8_t));

	LPC_USB->Ctrl = (READ_MODE | LOGICAL_ENDPOINT(logicalEndpoint));
	__NOP();
	__NOP();
	__NOP();

	uint32_t numberOfBytesReceived = LPC_USB->RxPLen & 0x3FF;
	bool isDataValid = LPC_USB->RxPLen & DATA_VALID_BIT;
	if (isDataValid) {
		uint32_t rxData;
		uint32_t numberOfDwordsToRead = (numberOfBytesReceived >> 2);
		while(counter < numberOfDwordsToRead) {
			rxData = LPC_USB->RxData;
			receivedData[counter*4]   = (rxData & 0x000000FF);
			receivedData[counter*4+1] = ((rxData & 0x0000FF00) >> 8);
			receivedData[counter*4+2] = ((rxData & 0x00FF0000) >> 16);
			receivedData[counter*4+3] = ((rxData & 0xFF000000) >> 24);
			counter += 1;
		}
	}

	LPC_USB->Ctrl = 0;

	uint8_t physicalEndpoint = logicalEndpoint << 1; // reading from OUT endpoint
	selectPhysicalEndpoint(physicalEndpoint);
	clearBuffer();

	return receivedData;
}

void sendDataToEndpoint(uint8_t logicalEndpoint, const uint8_t* buffer, uint8_t length) {
	uint8_t counter = 0;
	uint8_t physicalEndpoint = (logicalEndpoint << 1) + 1;

	selectPhysicalEndpoint(physicalEndpoint);

	LPC_USB->DevIntClr |= TxENDPKT;
	LPC_USB->Ctrl = (WRITE_MODE | LOGICAL_ENDPOINT(logicalEndpoint));
	__NOP();
	__NOP();
	__NOP();
	LPC_USB->TxPLen = (length & 0x3FF);

	while (counter < length) {
		uint32_t dwordToSend = 0;
		dwordToSend |= buffer[counter];
		dwordToSend |= (buffer[counter+1] << 8);
		dwordToSend |= (buffer[counter+2] << 16);
		dwordToSend |= (buffer[counter+3] << 24);
		LPC_USB->TxData = dwordToSend;
		counter += 4;
	}

	// to send empty packet we still need to write to register
	if (length == 0) {
		LPC_USB->TxData = 0x0;
	}

	LPC_USB->Ctrl = 0;

	// wait till the end of transfer
	while (!(LPC_USB->DevIntSt & TxENDPKT)) {}
	LPC_USB->DevIntClr |= TxENDPKT;

	selectPhysicalEndpoint(physicalEndpoint);
	validateTxBuffer();
}

void handleDeviceToHostRequests(uint8_t request, uint16_t value, uint16_t index, uint16_t length) {
	uint8_t descriptorType = UPPER_HALF_OF_WORD(value);
	switch (request) {
		case GET_DESCRIPTOR:
			switch(descriptorType) {
				case DEVICE_DESCRIPTOR_TYPE:
					sendDataToEndpoint(LOGICAL_CONTROL_ENDPOINT, DEVICE_DESCRIPTOR, length);

					#ifdef DEBUG_USB
					logDebugRequest('A', length);
					#endif
					break;
				case CONFIGURATION_DESCRIPTOR_TYPE:
					sendDataToEndpoint(LOGICAL_CONTROL_ENDPOINT, CONFIGURATION_DESCRIPTOR, length);

					#ifdef DEBUG_USB
					logDebugRequest('B', length);
					#endif
					break;
			}
			break;
		default:
			#ifdef DEBUG_USB
			logDebugRequest(0xEE, request);
			#endif
			break;
	}
}

void handleInterfaceToHostRequests(request, value, index, length) {
	uint8_t descriptorType = UPPER_HALF_OF_WORD(value);
	if (request == GET_DESCRIPTOR && descriptorType == GET_HID_REPORT_DESCRIPTOR) {
		sendDataToEndpoint(LOGICAL_CONTROL_ENDPOINT, HID_REPORT_DESCRIPTOR, length);

		#ifdef DEBUG_USB
		logDebugRequest('D', request);
		#endif
	}
}

void handleHostToDeviceRequests(uint8_t request, uint16_t value, uint16_t index, uint16_t length) {
	switch (request) {
		case CLEAR_FEATURE:
			sendDataToEndpoint(LOGICAL_CONTROL_ENDPOINT, NULL, 0);

			#ifdef DEBUG_USB
			logDebugMessage("CF");
			#endif
			break;
		case SET_CONFIGURATION:
			enableNoncontrolEndpoints();
			sendDataToEndpoint(LOGICAL_CONTROL_ENDPOINT, NULL, 0);

			#ifdef DEBUG_USB
			logDebugMessage("SC");
			#endif
			break;
		case SET_ADDRESS:
			addressAssignedByHost = value;
			writeToSIE(SET_ADDRESS_COMMAND, ADDRESS(addressAssignedByHost) | DEVICE_ENABLED);
			sendDataToEndpoint(LOGICAL_CONTROL_ENDPOINT, NULL, 0);

			#ifdef DEBUG_USB
			logDebugMessage("SA");
			#endif
			break;
		default:
			#ifdef DEBUG_USB
			logDebugRequest(0xDD, request);
			#endif
			break;
	}
}

void handleSetup(void) {
	uint8_t requestType = 0;
	uint8_t request = 0;
	uint16_t value = 0;
	uint16_t index = 0;
	uint16_t length = 0;

	uint8_t* data = readDataFromEndpoint(LOGICAL_CONTROL_ENDPOINT);
	requestType = data[0];
	request = data[1];
	value = data[2];
	value |= (data[3] << 8);
	index = data[4];
	index |= (data[5] << 8);
	length = data[6];
	length |= (data[7] << 8);

	switch (requestType) {
		case DEVICE_TO_HOST:
			handleDeviceToHostRequests(request, value, index, length);
			break;
		case INTEFRACE_TO_HOST:
			handleInterfaceToHostRequests(request, value, index, length);
			break;
		case HOST_TO_DEVICE:
			handleHostToDeviceRequests(request, value, index, length);
			break;
		default:
			#ifdef DEBUG_USB
			logDebugRequest(requestType, request);
			#endif
			break;
	}
}

const uint8_t inPacket[] = { 0x0, 0x1, 0x0, 0x0 };
void handleHIDReport(void) {
	sendDataToEndpoint(LOGICAL_HID_ENDPOINT, inPacket, 4);
}

bool lastPacketWasSetup(uint8_t packet) {
	return packet & SETUP_BIT;
}

void USB_IRQHandler(void) {
	uint32_t interrupts = 0;

	interrupts = LPC_USB->DevIntSt;
	LPC_USB->DevIntClr = interrupts;

	#ifdef DEBUG_USB
	uint8_t errorCode = getSIEerrorCode();
	if (errorCode > 0) {
		// do something
		// or just freak out
		logDebugRequest(0xEE, errorCode);
	}
	#endif

	if (interrupts & CONTROL_ENDPOINT_OUT) {
		uint8_t endpointInfo = selectEndpointClearInterrupt(0);
		if (lastPacketWasSetup(endpointInfo)) {
			handleSetup();
		}
	}

	if (interrupts & LOGICAL_IN_ENDPOINT_1) {
		selectEndpointClearInterrupt(3);
		handleHIDReport();
	}
}


int main(void) {
	setupUsb();

    while(1) {
    	__WFI();
    }
    return 0 ;
}
