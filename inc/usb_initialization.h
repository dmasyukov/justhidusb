/*
 * usb_initialization.h
 *
 *  Created on: Nov 20, 2014
 *      Author: denismasyukov
 */

#ifndef USB_INITIALIZATION_H_
#define USB_INITIALIZATION_H_

#include "chip.h"
#include "stdint.h"

#define VCCBUS		IOCON_PIO0_3
#define USBCONNECT	IOCON_PIO0_6

#define ENDPOINTS_ENABLED_FOR_INTERRUPTS								(EP0 | EP2 | EP3)

void setupUsb(void);

#endif /* USB_INITIALIZATION_H_ */
