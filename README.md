#Fake USB mouse on LPC1342/LPC1343

This is a very simple working example on how to build a USB device (mouse) for LPC134X microcontroller. I wish this code existed when I was looking for it, but there was none.
### My motivation to write this code:
* ROM-based USB examples are limited to MSC and HID, and there are more limitations to those classes as well
* LPCOpen 2.x doesn't have non-ROM USB examples for LPC134x
* The only example I could find (LPCOpen 1.x usbhid project) was overly complicated (tried to handle multiple USB classes, was hard to read, etc)

### Things worth noting:
* There is a `#define DEBUG` symbol definition that you can leave to enable logging into `debugStrings` variable. Or you can comment it out.
* It's very possible that the code right now is not 100% compliant with USB requirements because it doesn't respond to all necessary requests. It's up to you to add them or not.
* Things can be even more simplified from here:
> * Separate USB clock from main clock (both are 48 Mhz now)
> * Turn off unneccesary peripherals
> * Declutter `usb_definitions.h`
> * Simplify some code because I am NOT an awesome C developer

#### I hope that you will find this project a good starting point for a custom USB device. Pull requests are welcome.